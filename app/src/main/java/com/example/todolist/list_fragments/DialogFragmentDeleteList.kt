package com.example.todolist.item_fragments

import android.os.Bundle
import android.provider.BaseColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.example.todolist.R
import com.example.todolist.adapter.ListItemAdapter
import com.example.todolist.databinding.DialogFragmentDeleteBinding
import com.example.todolist.db.Contract.TODOItemEntry
import com.example.todolist.db.Contract.TODOListEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper

class DialogFragmentDeleteList(
    private val adapter: ListItemAdapter,
    private val position: Int,
    private val dbHelper: DatabaseSQLiteOpenHelper
) : androidx.fragment.app.DialogFragment() {

    private lateinit var binding: DialogFragmentDeleteBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DialogFragmentDeleteBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.buttons.findViewById<Button>(R.id.continueButton).setOnClickListener {
            val item = adapter.getItem(position)
            adapter.remove(item)

            val db = dbHelper.writableDatabase
            db.execSQL("""
                DELETE FROM ${TODOListEntry.TABLE_NAME}
                WHERE ${BaseColumns._ID}='${item?.id}'
                """.trimIndent()
            )
            db.execSQL("""
                DELETE FROM ${TODOItemEntry.TABLE_NAME}
                WHERE ${TODOItemEntry.COLUMN_LIST_ID}='${item?.id}'
            """.trimIndent())
            closeFragment()
        }
        binding.buttons.findViewById<Button>(R.id.cancelButton).setOnClickListener { closeFragment() }
    }

    private fun closeFragment() {
        requireActivity().supportFragmentManager.beginTransaction()
            .remove(this)
            .commit()
    }
}