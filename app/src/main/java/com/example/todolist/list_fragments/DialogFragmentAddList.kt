package com.example.todolist.list_fragments

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import com.example.todolist.R
import com.example.todolist.adapter.Item
import com.example.todolist.adapter.ListItemAdapter
import com.example.todolist.databinding.DialogFragmentInputBinding
import com.example.todolist.db.Contract.TODOListEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper
import com.google.android.material.snackbar.Snackbar

class DialogFragmentAddList(
    private val adapter: ListItemAdapter,
    private val dbHelper: DatabaseSQLiteOpenHelper
) : androidx.fragment.app.DialogFragment() {

    private lateinit var binding: DialogFragmentInputBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DialogFragmentInputBinding.inflate(inflater, container, false)
        binding.headerText.text = getString(R.string.add_todo_list)
        binding.positiveButton.text = getString(R.string.add)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.positiveButton.setOnClickListener {
            val listName = binding.input.text.toString().trim()
            val db = dbHelper.writableDatabase
            if(listName == "")
                return@setOnClickListener
            if(!existInDB(db, listName)) {
                val values = ContentValues()
                values.put(TODOListEntry.COLUMN_NAME, listName)
                val rowId = db.insert(TODOListEntry.TABLE_NAME, null, values)
                adapter.add(Item(listName, rowId))
            } else {
                showMessage()
            }
            closeFragment()
        }
        binding.cancelButton.setOnClickListener { closeFragment() }
    }

    private fun showMessage() {
        val message = "ToDo list with the following name already exists"
        Snackbar.make(activity?.findViewById(R.id.mainLayout)!!, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun existInDB(db: SQLiteDatabase, listName: String): Boolean {
        val cursor = db.rawQuery("""
                SELECT * FROM ${TODOListEntry.TABLE_NAME}
                WHERE ${TODOListEntry.COLUMN_NAME}='$listName'
            """.trimIndent(), null)
        return cursor.count > 0
    }

    private fun closeFragment() {
        requireActivity().supportFragmentManager.beginTransaction()
            .remove(this)
            .commit()
    }
}