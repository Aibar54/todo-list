package com.example.todolist.list_fragments

import android.os.Bundle
import android.provider.BaseColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.todolist.R
import com.example.todolist.adapter.ListItemAdapter
import com.example.todolist.databinding.DialogFragmentInputBinding
import com.example.todolist.db.Contract.TODOListEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper

class DialogFragmentUpdateList(
    private val adapter: ListItemAdapter,
    private val position: Int,
    private val dbHelper: DatabaseSQLiteOpenHelper
) : androidx.fragment.app.DialogFragment() {

    private lateinit var binding: DialogFragmentInputBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DialogFragmentInputBinding.inflate(inflater, container, false)
        binding.headerText.text = getString(R.string.update_todo_list)
        binding.positiveButton.text = getString(R.string.update)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.positiveButton.setOnClickListener {
            val text = binding.input.text.toString().trim()
            if (text == "") {
                return@setOnClickListener
            } else {
                val item = adapter.getItem(position)
                val db = dbHelper.writableDatabase

                adapter.remove(item)
                item?.name = text
                adapter.insert(item, position)

                db.execSQL("""
                    UPDATE ${TODOListEntry.TABLE_NAME}
                    SET ${TODOListEntry.COLUMN_NAME}='$text'
                    WHERE ${BaseColumns._ID}='${item?.id}'
                """.trimIndent())
            }
            closeFragment()
        }
        binding.cancelButton.setOnClickListener { closeFragment() }
    }

    private fun closeFragment() {
        requireActivity().supportFragmentManager.beginTransaction()
            .remove(this)
            .commit()
    }
}