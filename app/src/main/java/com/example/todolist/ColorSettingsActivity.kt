package com.example.todolist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.databinding.ActivityOptionsSettingsBinding

class ColorSettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOptionsSettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOptionsSettingsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setUpButtons()
    }

    private fun setUpButtons() {
        when(PrefsHelper.getCheckedColor(this)) {
            "green" -> binding.button1.isChecked = true
            "blue" -> binding.button2.isChecked = true
            "yellow" -> binding.button3.isChecked = true
        }
        when(PrefsHelper.getUncheckedColor(this)) {
            "brown" -> binding.button4.isChecked = true
            "pink" -> binding.button5.isChecked = true
            "violet" -> binding.button6.isChecked = true
        }
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked
            val color = view.text.toString()
            when (view.getId()) {
                R.id.button1 -> if (checked) PrefsHelper.setCheckedColor(this, color)
                R.id.button2 -> if (checked) PrefsHelper.setCheckedColor(this, color)
                R.id.button3 -> if (checked) PrefsHelper.setCheckedColor(this, color)
                R.id.button4 -> if (checked) PrefsHelper.setUncheckedColor(this, color)
                R.id.button5 -> if (checked) PrefsHelper.setUncheckedColor(this, color)
                R.id.button6 -> if (checked) PrefsHelper.setUncheckedColor(this, color)
                else -> return
            }
        }
    }

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, ColorSettingsActivity::class.java)
            context.startActivity(intent)
        }
    }

}