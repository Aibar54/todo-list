package com.example.todolist

import android.content.Context
import android.content.SharedPreferences

const val FIRST_COLOR_KEY = "FIRST_COLOR_key"
const val SECOND_COLOR_KEY = "SECOND_COLOR_key"

object PrefsHelper {
    private fun preferences(context: Context) : SharedPreferences =
        context.getSharedPreferences("default", 0)

    fun setCheckedColor(context: Context, color: String) {
        preferences(context).edit()
            .putString(FIRST_COLOR_KEY, color.lowercase())
            .apply()
    }

    fun getCheckedColor(context: Context) : String =
        preferences(context).getString(FIRST_COLOR_KEY, "yellow")!!
    
    fun setUncheckedColor(context: Context, color: String) {
        preferences(context).edit()
            .putString(SECOND_COLOR_KEY, color.lowercase())
            .apply()
    }

    fun getUncheckedColor(context: Context) : String =
        preferences(context).getString(SECOND_COLOR_KEY, "violet")!!
}