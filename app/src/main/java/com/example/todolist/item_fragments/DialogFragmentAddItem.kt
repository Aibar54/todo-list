package com.example.todolist.item_fragments

import android.content.ContentValues
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.todolist.adapter.ItemAdapter
import com.example.todolist.R
import com.example.todolist.adapter.Item
import com.example.todolist.databinding.DialogFragmentInputBinding
import com.example.todolist.db.Contract.TODOItemEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper

class DialogFragmentAddItem(
    private val adapter: ItemAdapter,
    private val listId: Long,
    private val dbHelper: DatabaseSQLiteOpenHelper
) : androidx.fragment.app.DialogFragment() {

    private lateinit var binding: DialogFragmentInputBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DialogFragmentInputBinding.inflate(inflater, container, false)
        binding.headerText.text = getString(R.string.add_todo_item)
        binding.positiveButton.text = getString(R.string.add)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.positiveButton.setOnClickListener {
            val text = binding.input.text.toString().trim()
            if( text == "")
                return@setOnClickListener
            else {
                val itemName = binding.input.text.toString()
                val db = dbHelper.writableDatabase
                val values = ContentValues().apply {
                    put(TODOItemEntry.COLUMN_NAME, itemName)
                    put(TODOItemEntry.COLUMN_LIST_ID, listId)
                    put(TODOItemEntry.COLUMN_IS_CHECKED, 0)
                }
                val rowId = db.insert(TODOItemEntry.TABLE_NAME, null, values)
                adapter.add(Item(itemName, rowId))
            }
            closeFragment()
        }
        binding.cancelButton.setOnClickListener { closeFragment() }
    }

    private fun closeFragment() {
        requireActivity().supportFragmentManager.beginTransaction()
            .remove(this)
            .commit()
    }
}