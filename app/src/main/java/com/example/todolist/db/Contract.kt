package com.example.todolist.db

import android.provider.BaseColumns

object Contract {

    const val CREATE_TODO_LIST_ENTRY_TABLE =
        "CREATE TABLE IF NOT EXISTS ${TODOListEntry.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${TODOListEntry.COLUMN_NAME} TEXT NOT NULL)"

    const val CREATE_TODO_ITEM_ENTRY_TABLE =
        "CREATE TABLE IF NOT EXISTS ${TODOItemEntry.TABLE_NAME} (" +
                "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                "${TODOItemEntry.COLUMN_NAME} TEXT NOT NULL," +
                "${TODOItemEntry.COLUMN_LIST_ID} INTEGER NOT NULL," +
                "${TODOItemEntry.COLUMN_IS_CHECKED} BOOLEAN NOT NULL)"



    object TODOListEntry : BaseColumns {
        const val TABLE_NAME = "todo_lists"
        const val COLUMN_NAME = "name"
    }

    object TODOItemEntry : BaseColumns {
        const val TABLE_NAME = "todo_items"
        const val COLUMN_NAME = "name"
        const val COLUMN_LIST_ID = "list_id"
        const val COLUMN_IS_CHECKED = "is_checked"
    }
}
