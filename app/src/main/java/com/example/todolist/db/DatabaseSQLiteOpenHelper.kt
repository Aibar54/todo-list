package com.example.todolist.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class DatabaseSQLiteOpenHelper(
    context: Context?,
) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        Log.i("abc123", "onCreate: create tables")
        db.execSQL(Contract.CREATE_TODO_LIST_ENTRY_TABLE)
        db.execSQL(Contract.CREATE_TODO_ITEM_ENTRY_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        Log.i("abc123", "onUpgrade: upg")
        db.execSQL("DROP TABLE IF EXISTS ${Contract.TODOListEntry.TABLE_NAME}")
        db.execSQL("DROP TABLE IF EXISTS ${Contract.TODOItemEntry.TABLE_NAME}")
        onCreate(db);
    }

    companion object {
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "todo_list.db"
    }
}