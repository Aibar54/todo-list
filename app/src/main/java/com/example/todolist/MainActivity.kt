package com.example.todolist

import android.os.Bundle
import android.provider.BaseColumns
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.adapter.Item
import com.example.todolist.adapter.ListItemAdapter
import com.example.todolist.databinding.ActivityMainBinding
import com.example.todolist.db.Contract.TODOListEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper
import com.example.todolist.list_fragments.DialogFragmentAddList

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: ListItemAdapter
    private lateinit var dbHelper: DatabaseSQLiteOpenHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        dbHelper = DatabaseSQLiteOpenHelper(this)

        adapter = ListItemAdapter(this)
        binding.list.adapter = adapter

        uploadUserData()

        binding.addButton.setOnClickListener {
            val dialogFragment = DialogFragmentAddList(adapter, dbHelper)
            dialogFragment.show(supportFragmentManager, "Add list fragment")
        }
        binding.list.setOnItemClickListener { parent, view, position, id ->
            ListViewActivity.start(this, adapter.getItem(position)!!)
        }
        binding.settingsButton.setOnClickListener {
            ColorSettingsActivity.start(this)
        }
    }

    private fun uploadUserData() {
        val db = dbHelper.readableDatabase
        val cursor = db.rawQuery(
            "SELECT * FROM ${TODOListEntry.TABLE_NAME}",
            null
        )
        with(cursor) {
            while (moveToNext()) {
                val listName = getString(getColumnIndexOrThrow(TODOListEntry.COLUMN_NAME))
                val listId = getLong(getColumnIndexOrThrow(BaseColumns._ID))
                adapter.add(Item(listName, listId))
            }
        }
    }

}