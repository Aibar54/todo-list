package com.example.todolist.adapter

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.provider.BaseColumns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.PrefsHelper
import com.example.todolist.R
import com.example.todolist.db.Contract.TODOItemEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper
import com.example.todolist.item_fragments.DialogFragmentDeleteItem
import com.example.todolist.item_fragments.DialogFragmentUpdateItem

class ItemAdapter(
    context: Context,
    private val objects: MutableList<Item> = mutableListOf()
) : ArrayAdapter<Item>(context, R.layout.todo_item, objects) {

    private val dbHelper = DatabaseSQLiteOpenHelper(context)
    private val colorChecked = when(PrefsHelper.getCheckedColor(context)) {
        "green" -> Color.GREEN
        "blue" -> Color.parseColor("#0000FF")
        else -> Color.YELLOW
    }
    private val colorUnchecked = when(PrefsHelper.getUncheckedColor(context)) {
        "brown" -> Color.parseColor("#964B00")
        "pink" -> Color.parseColor("#FFC0CB")
        else -> Color.parseColor("#FF6200EE")
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val convertViewCopy = convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.todo_item, null)

        val itemText = convertViewCopy.findViewById<TextView>(R.id.itemText)
        val checkBox = convertViewCopy.findViewById<CheckBox>(R.id.checkbox)
        val editButton = convertViewCopy.findViewById<ImageButton>(R.id.editButton)
        val deleteButton = convertViewCopy.findViewById<ImageButton>(R.id.deleteButton)

        val item = objects[position]
        itemText.text = item.name
        itemText.setTextColor(colorUnchecked)

        if(item.checked) {
            checkBox.isChecked = true
            itemText.setTextColor(colorChecked)
            checkBox.buttonTintList = ColorStateList.valueOf(Color.RED)
        }

        checkBox.setOnClickListener {
            if (checkBox.isChecked) {
                itemText.setTextColor(colorChecked)
                checkBox.buttonTintList = ColorStateList.valueOf(Color.RED)
                item.checked = true
                updateDB(item, 1)
            } else {
                itemText.setTextColor(colorUnchecked)
                checkBox.buttonTintList = ColorStateList.valueOf(Color.GRAY)
                item.checked = false
                updateDB(item, 0)
            }
        }
        editButton.setOnClickListener {
            val dialogFragment = DialogFragmentUpdateItem(this, position, dbHelper)
            dialogFragment.show((context as AppCompatActivity).supportFragmentManager, "Update fragment")
        }
        deleteButton.setOnClickListener {
            val dialogFragment = DialogFragmentDeleteItem(this, position, dbHelper)
            dialogFragment.show((context as AppCompatActivity).supportFragmentManager, "Delete fragment")
        }

        return convertViewCopy
    }

    private fun updateDB(item: Item, checked: Int) {
        val db = dbHelper.writableDatabase
        db.execSQL("""
                    UPDATE ${TODOItemEntry.TABLE_NAME}
                    SET ${TODOItemEntry.COLUMN_IS_CHECKED}='$checked'
                    WHERE ${BaseColumns._ID}='${item.id}'
                """.trimIndent())
    }
}
