package com.example.todolist.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.R
import com.example.todolist.db.DatabaseSQLiteOpenHelper
import com.example.todolist.item_fragments.DialogFragmentDeleteList
import com.example.todolist.list_fragments.DialogFragmentUpdateList


class ListItemAdapter(
    context: Context,
    private val objects: MutableList<Item> = mutableListOf()
) : ArrayAdapter<Item>(context, R.layout.todo_list_item, objects) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val convertViewCopy = convertView ?: LayoutInflater
            .from(parent.context)
            .inflate(R.layout.todo_list_item, null)

        val itemText = convertViewCopy.findViewById<TextView>(R.id.itemText)
        val optionsButton = convertViewCopy.findViewById<ImageButton>(R.id.optionsButton)

        itemText.isFocusable = false
        optionsButton.isFocusable = false

        val item = objects[position]
        itemText.text = item.name

        optionsButton.setOnClickListener {
            val popup = PopupMenu(context, optionsButton)
            val inflater: MenuInflater = popup.menuInflater
            inflater.inflate(R.menu.list_menu, popup.getMenu())
            popup.show()
            popup.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.edit_item -> {
                        val dialogFragment = DialogFragmentUpdateList(this, position, DatabaseSQLiteOpenHelper(context))
                        dialogFragment.show((context as AppCompatActivity).supportFragmentManager, "Update list fragment")
                    }
                    R.id.delete_item -> {
                        val dialogFragment = DialogFragmentDeleteList(this, position, DatabaseSQLiteOpenHelper(context))
                        dialogFragment.show((context as AppCompatActivity).supportFragmentManager, "Delete list fragment")
                    }
                }
                return@setOnMenuItemClickListener true
            }
        }

        return convertViewCopy
    }
}