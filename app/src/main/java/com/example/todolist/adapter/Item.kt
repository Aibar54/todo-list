package com.example.todolist.adapter

data class Item(var name: String, val id: Long, var checked: Boolean = false)