package com.example.todolist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.BaseColumns
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.todolist.adapter.Item
import com.example.todolist.adapter.ItemAdapter
import com.example.todolist.databinding.ActivityListViewBinding
import com.example.todolist.db.Contract.TODOItemEntry
import com.example.todolist.db.DatabaseSQLiteOpenHelper
import com.example.todolist.item_fragments.DialogFragmentAddItem
import kotlinx.coroutines.Dispatchers

class ListViewActivity : AppCompatActivity() {

    private lateinit var binding: ActivityListViewBinding
    private lateinit var adapter: ItemAdapter
    private lateinit var dbHelper: DatabaseSQLiteOpenHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityListViewBinding.inflate(layoutInflater)
        dbHelper = DatabaseSQLiteOpenHelper(this)
        setContentView(binding.root)
        setSupportActionBar(binding.toolBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = listItem.name

        adapter = ItemAdapter(this)
        binding.list.adapter = adapter

        uploadUserData()

        binding.addButton.setOnClickListener {
            val dialogFragment = DialogFragmentAddItem(adapter, listItem.id, dbHelper)
            dialogFragment.show(supportFragmentManager, "Add item fragment")
        }

    }

    private fun uploadUserData() = with(Dispatchers.IO) {
        val db = dbHelper.readableDatabase

        val cursor = db.rawQuery("""
            SELECT * FROM ${TODOItemEntry.TABLE_NAME}
            WHERE ${TODOItemEntry.COLUMN_LIST_ID}='${listItem.id}'
        """.trimIndent(),
            null
        )
        with(cursor) {
            while(moveToNext()) {
                val itemName = getString(getColumnIndexOrThrow(TODOItemEntry.COLUMN_NAME))
                val itemId = getLong(getColumnIndexOrThrow(BaseColumns._ID))
                val isChecked = getInt(getColumnIndexOrThrow(TODOItemEntry.COLUMN_IS_CHECKED))
                adapter.add(Item(itemName, itemId, isChecked > 0))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home){
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        private lateinit var listItem: Item

        fun start(context: Context, item: Item) {
            listItem = item
            val intent = Intent(context, ListViewActivity::class.java)
            context.startActivity(intent)
        }
    }
}